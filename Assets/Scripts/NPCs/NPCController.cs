﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCController : MonoBehaviour {

	public bool withDialog = false;
	[SerializeField]
	public NPCDialog npcDialog;
}

[System.Serializable]
public class NPCDialog{
	public Sprite leftCharActive;
	public Sprite leftCharInactive;
	public Sprite rightCharActive;
	public Sprite rightCharInactive;
}
