using System;

[Serializable]
public class DialogCollection{
    public Dialog[] dialogs;

    public override string ToString(){
        string result = "DIALOGS\n";
        foreach (var dialog in dialogs){
            result += string.Format("Dialog: {0}\ntext: {1}\noption1: {2}\noption2: {3}\ntalkingCharacter: {4}\n\n", dialog.text, dialog.option1, dialog.option2, dialog.talkingCharacter);
        }
        return result;
    }
}