﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

	public GameObject collidingWithNPCObj = null;
	public GameObject dialogObj = null;

	void Awake(){
		collidingWithNPCObj.SetActive(false);
	}

	public void StartDialog(DialogController.DialogsIDs dialogID, NPCDialog npcDialog){
		collidingWithNPCObj.SetActive(false);
		dialogObj.SetActive(true);
		if(!dialogObj.GetComponent<DialogController>().init){
			dialogObj.GetComponent<DialogController>().Setup(npcDialog);
		}
		dialogObj.GetComponent<DialogController>().StartDialog(dialogID);
	}
	public void OnEndDialog(){
		dialogObj.SetActive(false);
	}
}
