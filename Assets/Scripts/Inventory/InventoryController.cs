﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryController : MonoBehaviour {

	// Public properties
	[HideInInspector]
	public List<ItemSO> playerItems = new List<ItemSO>();
	[HideInInspector]
	public ItemContainer itemContainerOpened = null;
	[HideInInspector]
	public Inventory inventorySelected = null;
	[HideInInspector]
	public bool switchInventory = false;

	// Private properties
	[SerializeField]
	private GameObject canvas;
	[SerializeField]
	private GameObject slotPf;
	[SerializeField]
	private InventoryType[] inventoryTypes;
	[SerializeField]
	private InventoryType nonBreakableContainerInventory;
	[SerializeField]
	private GameObject draggingSlotObj = null;
	[SerializeField]
	private GameObject worldItemPf = null;

	private InventoryType inventoryActive;

	// Raycast
	GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

	// Drag and drop
	InventorySlot slotOnBeginDrag = null;

	/// <summary>
	/// Enum for every type of inventory (player and others)
	/// </summary>
	public enum InventoryTypeID{
		Small = 0,
		Medium = 1,
		Large = 2,
		NonBreakableContainer = 3,
	};

	void Awake(){
		// Raycast
        m_Raycaster = canvas.GetComponent<GraphicRaycaster>();
        m_EventSystem =FindObjectOfType<EventSystem>();

		draggingSlotObj.SetActive(false);
		// Find the player inventory type and initialize it
		foreach (InventoryType inventory in inventoryTypes){
			if(GameController.inventoryTypePlayer == inventory.inventoryTypeID){
				inventory.obj.SetActive(true);
				inventory.obj.GetComponent<Inventory>().numberOfSlots = inventory.numberOfslots;
				inventory.obj.GetComponent<Inventory>().slotPf = slotPf;
				inventory.obj.GetComponent<Inventory>().inventoryController = this;
				inventory.obj.GetComponent<Inventory>().isContainerInventory = false;
				inventory.obj.GetComponent<Inventory>().Setup();
				inventoryActive = inventory;
				inventory.obj.SetActive(false);
			} else{
				// Hide others
				inventory.obj.SetActive(false);
			}
		}
		
		// Setup the item containers inventory
		nonBreakableContainerInventory.obj.SetActive(true);
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().numberOfSlots = nonBreakableContainerInventory.numberOfslots;
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().slotPf = slotPf;
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().inventoryController = this;
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().isContainerInventory = true;
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().Setup();
		nonBreakableContainerInventory.obj.SetActive(false);
		// Initialize players item list
		playerItems = new List<ItemSO>(new ItemSO[inventoryActive.numberOfslots]);
	}

	void LateUpdate(){
		if(switchInventory){
			SwitchInventory();
			switchInventory = false;
		}
	}

	/// <summary>
	/// Called from the inventory on select slot (from player inventory or others)
	/// </summary>
	/// <param name="item">The item selected</param>
	/// <param name="slot">The inventory slot selected</param>
	public void OnSelectSlot(ItemSO item, InventorySlot slot = null){
		// If the items container inventory is open...
		if(IsNonBreakableContainerOpen()){
			// Switch item inventory
			Inventory nonBreakableInv= nonBreakableContainerInventory.obj.GetComponent<Inventory>();
			int id = -1;
			// Find the first null index on container inventory
			id = nonBreakableInv.slots.FindIndex(i => i.itemOnSlot == null);
			if(id == -1){
				Debug.Log("NonBreakable container inventory full!");
			} else{
				// Manage the world item container...
				if(id > itemContainerOpened.items.Count-1){
					itemContainerOpened.items.Add(slot.itemOnSlot);
				} else{
					itemContainerOpened.items[id] = slot.itemOnSlot;
				}
				// Move the item to the container inventory and initialize it
				nonBreakableInv.slots[id].itemOnSlot = slot.itemOnSlot;
				nonBreakableInv.slots[id].containerID = id;
				nonBreakableInv.slots[id].Setup();
			}
		} else{
			// Selected item from the player inventory without other inventory opened
			// Close inventory
			inventoryActive.obj.SetActive(false);
			// Say it to the player
			FindObjectOfType<PlayerController>().OnSelectItemFromInventory(item);
		}
	}
	/// <summary>
	/// Called from player to close the inventory
	/// </summary>
	public void CloseInventory(){
		// Close inventory(ies)
		inventoryActive.obj.SetActive(false);
		if(nonBreakableContainerInventory.obj.activeInHierarchy){
			nonBreakableContainerInventory.obj.SetActive(false);
		}
		// Say it to the player
		FindObjectOfType<PlayerController>().OnInventoryClosed();
	}
	/// <returns>Returns inventory object active state</returns>
	public bool Toggle(){
		RectTransform rect = inventoryActive.obj.GetComponent<RectTransform>();
		rect.anchoredPosition = new Vector2(0f, rect.anchoredPosition.y);
		inventoryActive.obj.SetActive(!inventoryActive.obj.activeInHierarchy);
		if(nonBreakableContainerInventory.obj.activeInHierarchy){
			nonBreakableContainerInventory.obj.SetActive(false);
		}
		inventorySelected = inventoryActive.obj.GetComponent<Inventory>();
		inventorySelected.OnSelectThisInventory();
		return inventoryActive.obj.activeInHierarchy;
	}
	/// <summary>
	/// Called from player on pick item from the world
	/// </summary>
	/// <param name="item">The item to be picked</param>
	/// <returns>Returns false if cannot add item on inventory</returns>
	public bool OnPickItem(ItemSO item, int destID = -1){
		// Find the first null index on player inventory
		int id = GetPlayerInventoryFirstIndexWithoutItem();
		if(id == -1){
			return false;
		} else{
			// Set item to player inventory
			if(destID == -1){
				playerItems[id] = item;
				inventoryActive.obj.GetComponent<Inventory>().OnItemPicked(id);
			} else{
				playerItems[destID] = item;
				inventoryActive.obj.GetComponent<Inventory>().OnItemPicked(destID);
			}
			return true;
		}
	}
	/// <returns>Returns inventory object active state</returns>
	public bool ToggleNonBreakableContainer(ItemContainer container = null){
		inventoryActive.obj.SetActive(!inventoryActive.obj.activeInHierarchy);
		RectTransform rect = inventoryActive.obj.GetComponent<RectTransform>();
		rect.anchoredPosition = new Vector2(-500f, rect.anchoredPosition.y);
		nonBreakableContainerInventory.obj.SetActive(!nonBreakableContainerInventory.obj.activeInHierarchy);
		itemContainerOpened = container;
		nonBreakableContainerInventory.obj.GetComponent<Inventory>().OpenContainerInventory(container);
		inventorySelected = nonBreakableContainerInventory.obj.GetComponent<Inventory>();
		inventorySelected.OnSelectThisInventory();
		return nonBreakableContainerInventory.obj.activeInHierarchy;
	}
	/// <returns>Returns container inventory active state</returns>
	public bool IsNonBreakableContainerOpen(){
		return nonBreakableContainerInventory.obj.activeInHierarchy;
	}
	/// <returns>Returns the player's inventory first index without item, returns -1 if there's no more space</returns>
	public int GetPlayerInventoryFirstIndexWithoutItem(){
		return playerItems.FindIndex(i=>i==null);
	}

	/// <summary>
	/// Called from an inventory slot when its about to be dragged
	/// </summary>
	/// <param name="slot">The slot that is about to be dragged</param>
	public void OnBeginDrag(InventorySlot slot){
		slotOnBeginDrag = slot;
		draggingSlotObj.GetComponent<Image>().sprite = slotOnBeginDrag.itemOnSlot.inventoryImage;
		draggingSlotObj.SetActive(true);
	}
	/// <summary>
	/// Called from an inventory slot that is being dragged
	/// </summary>
	/// <param name="pointerPos">The position of the mouse on the screen</param>
	public void OnDrag(Vector2 pointerPos){
		draggingSlotObj.GetComponent<RectTransform>().position = pointerPos;
		//Set up the new Pointer Event
		m_PointerEventData = new PointerEventData(m_EventSystem);
		//Set the Pointer Event Position to that of the mouse position
		m_PointerEventData.position = Input.mousePosition;

		//Create a list of Raycast Results
		List<RaycastResult> results = new List<RaycastResult>();

		//Raycast using the Graphics Raycaster and mouse position
		m_Raycaster.Raycast(m_PointerEventData, results);

		foreach (RaycastResult result in results){
			if(result.gameObject.tag == "InventorySlot"){
				InventorySlot slot = result.gameObject.GetComponent<InventorySlot>();
				result.gameObject.transform.parent.parent.GetComponent<Inventory>().SetSlotSelected(slot.transform.GetSiblingIndex());
			}
		}
	}
	/// <summary>
	/// Called from an inventory slot when the dragging ends
	/// </summary>
	/// <param name="pointerPos">The position of the mouse on the screen</param>
	public void OnEndDrag(Vector2 pointerPos){
		//Set up the new Pointer Event
		m_PointerEventData = new PointerEventData(m_EventSystem);
		//Set the Pointer Event Position to that of the mouse position
		m_PointerEventData.position = Input.mousePosition;

		//Create a list of Raycast Results
		List<RaycastResult> results = new List<RaycastResult>();

		//Raycast using the Graphics Raycaster and mouse position
		m_Raycaster.Raycast(m_PointerEventData, results);

		GameObject res = null;
		foreach (RaycastResult result in results){
			if(result.gameObject.tag == "InventorySlot"){
				res = result.gameObject;
				InventorySlot slot = res.GetComponent<InventorySlot>();
				Inventory inv = result.gameObject.transform.parent.parent.GetComponent<Inventory>();
				if(slotOnBeginDrag.transform.GetSiblingIndex() == slot.transform.GetSiblingIndex()
					&&
					slotOnBeginDrag.transform.parent.parent == inv.transform){
					// Cancel the drag if releases on the same slot
					draggingSlotObj.SetActive(false);
					return;
				}
				inv.OnDropItem(slotOnBeginDrag, slot.transform.GetSiblingIndex());
				break;
			}
		}
		if(res){
			slotOnBeginDrag.transform.parent.parent.GetComponent<Inventory>().RemoveItemOnSlot(slotOnBeginDrag);
		} else{
			// Drop on the floor
			ItemSO item = ScriptableObject.CreateInstance(typeof(ItemSO)) as ItemSO;
			item = slotOnBeginDrag.itemOnSlot;
			DropItemOnTheFloor(item);
			slotOnBeginDrag.transform.parent.parent.GetComponent<Inventory>().RemoveItemOnSlot(slotOnBeginDrag);
		}
		
		draggingSlotObj.SetActive(false);
	}
	/// <summary>
	/// Instantiate an item on the world
	/// </summary>
	/// <param name="item">The item to instantiate</param>
	void DropItemOnTheFloor(ItemSO item){
		GameObject player = FindObjectOfType<PlayerController>().gameObject;
		Vector3 pos = player.transform.position;
		pos.x += Random.Range(-0.3f, 0.3f);
		pos.y += Random.Range(-0.3f, 0.3f);
		GameObject _item = Instantiate(worldItemPf, pos, Quaternion.identity, GameObject.Find("Items").transform);
		_item.GetComponent<Item>().item = item;
	}

	void SwitchInventory(){
		if(nonBreakableContainerInventory.obj.activeInHierarchy){
			inventorySelected = (inventorySelected == nonBreakableContainerInventory.obj.GetComponent<Inventory>())
								?
								inventoryActive.obj.GetComponent<Inventory>()
								:
								nonBreakableContainerInventory.obj.GetComponent<Inventory>();
			inventorySelected.OnSelectThisInventory();
		}
	}
}

/// <summary>
/// Struct for every inventory type (player or others)
/// </summary>
[System.Serializable]
public struct InventoryType{
	public GameObject obj;
	public int numberOfslots;
	public InventoryController.InventoryTypeID inventoryTypeID;
}
