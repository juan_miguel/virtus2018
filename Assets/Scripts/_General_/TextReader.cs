﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TextReader : MonoBehaviour {

	[SerializeField]
	private string key = null;

	TextMeshProUGUI tmproTxt = null;
	Text txt = null;

	void Awake(){
		if(GetComponent<TextMeshProUGUI>()){
			tmproTxt = GetComponent<TextMeshProUGUI>();
			tmproTxt.text = Texts.texts[key];
		} else{
			txt = GetComponent<Text>();
			txt.text = Texts.texts[key];
		}
	}
}
