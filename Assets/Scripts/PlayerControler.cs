﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour {

	public MapaControler mapaControler;

	public float velocidad;
	public Camera camara;

	private Rigidbody2D rb;
	private Animator anim;
	private Vector2 pos = new Vector2(-10f,-10f),posOld = new Vector2(0f,0f), posV = new Vector2(0f,0f);
	private Vector3 posCamara;
	private readonly int ATAQUEBASICO = 1;
	private readonly int ATAQUEASESINO = 2;
	private readonly int ATAQUESWORD = 3;
	private int accionEspecialHeroe=0,tipoAtaque;
	private string nombreAni="",nombreAniOld="",direccionHeroe="";
	private UIController uiController;
	private bool watchingInventory = false;
	private GameObject lastObjectTriggered = null;


	void Start (){
		//Crea variables
		rb = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		uiController = FindObjectOfType<UIController>();
		posCamara = new Vector3 (0f, 0f, 0f);
		tipoAtaque = ATAQUEBASICO;
		//Asigna la posicion 
		rb.MovePosition(pos);
		//Animacion inicial del heroe
		direccionHeroe="_aba";
		accionEspecialHeroe = 3+(int)(0.58f/Time.deltaTime);
		nombreAni="ninjaaparecer"+direccionHeroe;
		anim.Play(nombreAni);
		nombreAniOld = nombreAni;
	}



	//Para recolocar el ninja
	public void MoverNinja(float x,float y)
	{
		pos.x = x;
		pos.y = y;
	}

	//Ajusta colision y tamaño al ninja
	private int CasillaXY_ninja(float x,float y)
	{
		return mapaControler.CasillaXY_rango(x, y, 0.3f, 0f, -0.25f);
		//return mapaControler.CasillaXY_rango(pos.x, pos.y, 0.3f, 0f, 0f);

	}

	//Lo mueve pero antes comprueba si colisione
	public void DesplazarNinjaColision(float vx, float vy){
		pos.x += vx;
		pos.y += vy;
		//Limitamos area de movimiento
		pos.x = Mathf.Clamp(pos.x,0,MapaControler.Ancho_Mapa);
		pos.y = Mathf.Clamp(pos.y,0,MapaControler.Alto_Mapa);
		//Aqui comprobamos que no es una casilla ocupada
		if (CasillaXY_ninja (pos.x, pos.y) == mapaControler.TILE_OCUPADO ){
			//Moverse solo en X
			if (CasillaXY_ninja (pos.x, posOld.y) == mapaControler.TILE_NIVEL1) {
				pos.y = posOld.y;
			}
			else 
			//Moverse solo en Y
			if (CasillaXY_ninja (posOld.x, pos.y) == mapaControler.TILE_NIVEL1){
				pos.x = posOld.x;			
			}
			else {
				pos.x = posOld.x;
				pos.y = posOld.y;
			}
		}

	}

	void Update(){
		if(Input.GetButtonDown ("ActionButton")){
			OnPressActionButton();
		} else if (Input.GetButtonDown("InventoryToggleButton")){
			OnToggleInventory();
		}
	}

	void LateUpdate (){
		float sizeCamara = camara.orthographicSize;
		//Aqui actualizamos la camara
		posCamara.x=transform.position.x;
		posCamara.y=transform.position.y;
		posCamara.z=camara.transform.position.z;
		posCamara.x = Mathf.Clamp(posCamara.x,sizeCamara,MapaControler.Ancho_Mapa-sizeCamara);
		posCamara.y = Mathf.Clamp(posCamara.y,sizeCamara,MapaControler.Alto_Mapa-sizeCamara);
		camara.transform.position = posCamara;

	}

	void FixedUpdate(){
		//Contadores de accion
		if (accionEspecialHeroe > 0)
			accionEspecialHeroe--;
		//Block movement
		if (accionEspecialHeroe > 5 || uiController.dialogObj.activeInHierarchy || watchingInventory) {
			//DesplazarNinjaColision (posV.x, posV.y);
			//rb.MovePosition(pos);
			if(accionEspecialHeroe <= 5)
				anim.Play("ninjastand"+direccionHeroe);
		}
		else{
			//Movimiento
			float mX = Input.GetAxis("Horizontal");
			float mY = Input.GetAxis("Vertical");
			//
			posV.x=mX * velocidad;
			posV.y=mY * velocidad;
			DesplazarNinjaColision (posV.x, posV.y);
			//Test para mejorar la animacion
			if (pos.x == posOld.x)
				mX = 0;
			if (pos.y == posOld.y)
				mY = 0;
			//
			rb.MovePosition(pos);
			//Aqui controlamos la animacion
			bool moviendoseAux=false;
			if ((Mathf.Abs (mX) > 0.05) || (Mathf.Abs (mY) > 0.05))
				moviendoseAux = true;
			if (moviendoseAux==true) {
				if (mY > 0f)
					direccionHeroe = "_arr";
				if (mY < 0f)
					direccionHeroe = "_aba";
				if (mX < 0f)
					direccionHeroe = "_izq";
				if (mX > 0f)
					direccionHeroe = "_der";
				nombreAni = "ninjaanimation_2" + direccionHeroe;
			}
			else{
				//nombreTrigger=nombreTrigger.Replace("ninjaanimation_2","ninjastand");
				nombreAni="ninjastand"+direccionHeroe;
			}
			//Aqui realizamos el ataque
			if (Input.GetButton ("Fire1")&&(accionEspecialHeroe==0)){
				posV.x = 0f;
				posV.y = 0f;
				if (tipoAtaque == ATAQUEBASICO)
				{
					accionEspecialHeroe = 3+(int)(0.67f/Time.deltaTime);
					nombreAni="ninjaattack"+direccionHeroe;
				}
				if (tipoAtaque == ATAQUEASESINO)
				{
					accionEspecialHeroe = 3+(int)(0.25f/Time.deltaTime);
					nombreAni="ninjaassasination"+direccionHeroe;
				}
				if (tipoAtaque == ATAQUESWORD)
				{
					accionEspecialHeroe = 3+(int)(0.67f/Time.deltaTime);
					nombreAni="ninjasword"+direccionHeroe;
				}
				//Cambia el tipo de ataque, QUITAR!
				tipoAtaque++;
				if (tipoAtaque > ATAQUESWORD)
					tipoAtaque = ATAQUEBASICO;
			}
			else
			//Aqui realizamos ataque a distancia
			if (Input.GetButton ("Fire2")&&(accionEspecialHeroe==0)){
				posV.x = 0f;
				posV.y = 0f;
				accionEspecialHeroe = 3+(int)(0.67f/Time.deltaTime);
				nombreAni="ninjabow"+direccionHeroe;
			}	
			else
			//Aqui realizamos el salto
			if (Input.GetButton ("Fire3")&&(accionEspecialHeroe==0)){
				//Duplica la velocidad!
				posV.x=posV.x*2f;
				posV.y=posV.y*2f;
				accionEspecialHeroe = 3+(int)(0.50f/Time.deltaTime);
				nombreAni="ninjavoltereta"+direccionHeroe;
			}
			else
			//Aqui realizamos la magia
			if (Input.GetButton ("Fire4")&&(accionEspecialHeroe==0)){
				posV.x = 0f;
				posV.y = 0f;
				accionEspecialHeroe = 3+(int)(0.67f/Time.deltaTime);
				nombreAni="ninjamagic"+direccionHeroe;
			}
		}
		if( (nombreAni.Length > 0) && (nombreAni!=nombreAniOld) )
		{
			anim.Play(nombreAni);
			nombreAniOld = nombreAni;
		}
		//Actualizamos las posiciones del frame anterior
		posOld.x=pos.x;
		posOld.y=pos.y;
	}

	void OnPressActionButton(){
		if(lastObjectTriggered != null){
			if(uiController.collidingWithNPCObj.activeInHierarchy && !uiController.dialogObj.activeInHierarchy){
				// Talk with NPC
				uiController.StartDialog(DialogController.DialogsIDs.test, lastObjectTriggered.GetComponent<NPCController>().npcDialog);
			} else if (lastObjectTriggered.tag == "Item"){
				// Pick item
				lastObjectTriggered.GetComponent<Item>().OnPick();
			}
		}
	}
	void OnToggleInventory(){
		watchingInventory = FindObjectOfType<InventoryController>().Toggle();
	}
	void OnTriggerEnter2D(Collider2D other){
		switch (other.tag){
			case "NPC":
			if(other.GetComponent<NPCController>().withDialog)
				uiController.collidingWithNPCObj.SetActive(true);
			break;
			case "Item":
			break;
		}
		lastObjectTriggered = other.gameObject;
	}
	void OnTriggerExit2D(Collider2D other){
		switch (other.tag){
			case "NPC":
			uiController.collidingWithNPCObj.SetActive(false);
			break;
		}
	}

	public void OnSelectItemFromInventory(ItemSO itemSelected){
		if(itemSelected != null){
			Debug.Log("Item selected with ID: " + itemSelected.itemID);
		}
		watchingInventory = false;
	}
	/*
	 * 
	 * 	//Control de la nave
	public string horizontal="Horizontal",vertical="Vertical",fire1="Fire1";
	//Variables de la nave
	public float velocidad;
	public float limiteX,limiteY;
	public Transform heroeSitioDisparo;
	public Object disparo;
	//
	private Rigidbody rb;
	private Vector3 pos;
	//private Vector3 movement;

	// Use this for initialization
	void Start () {
		//Crea variables
		rb = GetComponent<Rigidbody>();
		pos = new Vector3(0f, 0f, 0f);
		//Asigna la posicion de creacion
		pos = rb.position;
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate()
	{
		//Movimiento
		float mX = Input.GetAxis(horizontal);
		float mY = Input.GetAxis(vertical);
		pos.x += mX;
		pos.y = 0f;
		pos.z += mY;
		pos.x = Mathf.Clamp(pos.x, -limiteX, limiteX);
		pos.z = Mathf.Clamp(pos.z, -limiteY, limiteY);
		//rb.transform.eulerAngles = new Vector3(0, 180, mX*30f);
		rb.MoveRotation(Quaternion.Euler(0, 180, mX*30f));
		rb.MovePosition(pos);


		if (Input.GetButtonDown(fire1))
		{
			Instantiate (disparo, heroeSitioDisparo);
		}
	 * 
	 * 
	 */



}
