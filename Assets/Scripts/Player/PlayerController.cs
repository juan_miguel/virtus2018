﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

public class PlayerController : MonoBehaviour {

	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	[SerializeField]
	private Transform attackZonesParent = null;

	[HideInInspector]
	private float normalizedHorizontalSpeed = 0, normalizedVerticalSpeed = 0;

	private CharacterController2D _controller;
	private Animator _animator;
	private RaycastHit2D _lastControllerColliderHit;
	private Vector3 _velocity;
	private bool watchingInventory = false;
	private UIController uiController;
	private GameObject lastObjectTriggered = null, lastNPCTriggered = null;
	private bool animatingAttack = false;

	void Awake(){
		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();
		uiController = FindObjectOfType<UIController>();

		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		_controller.onTriggerStayEvent += onTriggerStayEvent;
		_controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	void Update(){
		if(Input.GetMouseButtonDown(0)){
			FindObjectOfType<Navigation4Tilemap.Navigation4Tilemap>().OnSetPos();
		}
		if(Input.GetButtonDown ("ActionButton")){
			OnPressActionButton();
		} else if (Input.GetButtonDown("InventoryToggleButton")){
			OnToggleInventory();
		}
		_controller.IsGrounded = true;

		normalizedHorizontalSpeed = normalizedVerticalSpeed = 0;

		_animator.SetBool("Run", false);
		if(!watchingInventory && !uiController.dialogObj.activeInHierarchy){
			AttackControl();
			MovementControl();
		}
	}

	void AttackControl(){
		if(_animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1f && _animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")){
			_animator.ResetTrigger("Attack");
			_animator.SetFloat("BowAttack", 0);
			_animator.SetFloat("AssassinationAttack", 0);
			_animator.SetFloat("NormalAttack", 0);
			_animator.SetFloat("MagicAttack", 0);
			_animator.SetFloat("SwordAttack", 0);
			_animator.SetTrigger("StopAttack");
		}

		Collider2D col = null;

		if(Input.GetButtonDown("Attack")){
			_animator.SetTrigger("Attack");
			_animator.SetFloat("NormalAttack", 1);
			_animator.SetFloat("AssassinationAttack", 0);
			_animator.SetFloat("BowAttack", 0);
			_animator.SetFloat("MagicAttack", 0);
			_animator.SetFloat("SwordAttack", 0);
			_animator.ResetTrigger("StopAttack");
			if(_animator.GetFloat("Vertical") == 1){
				// Attack up
				col = Physics2D.OverlapBox(attackZonesParent.GetChild(0).GetChild(1).position, attackZonesParent.GetChild(0).GetChild(1).GetComponent<BoxCollider2D>().size, 0f);
			} else if(_animator.GetFloat("Vertical") == -1){
				// Attack down
				col = Physics2D.OverlapBox(attackZonesParent.GetChild(0).GetChild(2).position, attackZonesParent.GetChild(0).GetChild(1).GetComponent<BoxCollider2D>().size, 0f);
			} else{
				// Attack horizontal (front)
				col = Physics2D.OverlapBox(attackZonesParent.GetChild(0).GetChild(0).position, attackZonesParent.GetChild(0).GetChild(1).GetComponent<BoxCollider2D>().size, 0f);
			}
		} else if(Input.GetButtonDown("RangeAttack")){
			_animator.SetTrigger("Attack");
			_animator.SetFloat("BowAttack", 1);
			_animator.SetFloat("AssassinationAttack", 0);
			_animator.SetFloat("NormalAttack", 0);
			_animator.SetFloat("MagicAttack", 0);
			_animator.SetFloat("SwordAttack", 0);
			_animator.ResetTrigger("StopAttack");
		} else if(Input.GetButtonDown("MagicAttack")){
			_animator.SetTrigger("Attack");
			_animator.SetFloat("MagicAttack", 1);
			_animator.SetFloat("AssassinationAttack", 0);
			_animator.SetFloat("NormalAttack", 0);
			_animator.SetFloat("BowAttack", 0);
			_animator.SetFloat("SwordAttack", 0);
			_animator.ResetTrigger("StopAttack");
		}

		if(col && col.tag == "ItemContainer"){
			ItemContainer container = col.GetComponent<ItemContainer>();
			if(container.breakable){
				container.OnBreakContainer();
			}
		}
	}

	void MovementControl(){
		if(Input.GetAxisRaw("Vertical")>0){
			normalizedVerticalSpeed = 1;
			_animator.SetBool("Run", true);
			_animator.SetFloat("Horizontal", 0);
			_animator.SetFloat("Vertical", 1);
		}
		else if(Input.GetAxisRaw("Vertical")<0){
			normalizedVerticalSpeed = -1;
			_animator.SetBool("Run", true);
			_animator.SetFloat("Horizontal", 0);
			_animator.SetFloat("Vertical", -1);
		}
		if(Input.GetAxisRaw("Horizontal")>0f){
			normalizedHorizontalSpeed = 1;
			if( transform.localScale.x < 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

			_animator.SetBool("Run", true);
			_animator.SetFloat("Horizontal", 0);
			_animator.SetFloat("Vertical", 0);
			transform.localScale = new Vector3(1,1,1);
		}
		else if(Input.GetAxisRaw("Horizontal")<0){
			normalizedHorizontalSpeed = -1;

			_animator.SetBool("Run", true);
			_animator.SetFloat("Horizontal", 0);
			_animator.SetFloat("Vertical", 0);
			transform.localScale = new Vector3(-1,1,1);
		}
		if(normalizedHorizontalSpeed != 0 || normalizedVerticalSpeed != 0){
			_animator.ResetTrigger("Attack");
			_animator.SetFloat("BowAttack", 0);
			_animator.SetFloat("AssassinationAttack", 0);
			_animator.SetFloat("NormalAttack", 0);
			_animator.SetFloat("MagicAttack", 0);
			_animator.SetFloat("SwordAttack", 0);
			_animator.SetTrigger("StopAttack");
		}


		// apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
		var smoothedMovementFactor = _controller.IsGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );
		_velocity.y = Mathf.Lerp( _velocity.y, normalizedVerticalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		_controller.move( _velocity * Time.deltaTime );

		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;
	}

	public void OnSelectItemFromInventory(ItemSO itemSelected){
		if(itemSelected != null){
			Debug.Log("Item selected with ID: " + itemSelected.itemID);
			if(itemSelected.consumable){
				switch (itemSelected.consumableType){
					case ItemConsumableTypes.healing:
					Debug.Log("Healing item consumed");
					break;
				}
			}
		}
		watchingInventory = false;
	}
	public void OnInventoryClosed(){
		watchingInventory = false;
	}
	void OnPressActionButton(){
		if(lastObjectTriggered != null && !uiController.dialogObj.activeInHierarchy){
			if(uiController.collidingWithNPCObj.activeInHierarchy){
				// Talk with NPC
				uiController.StartDialog(DialogController.DialogsIDs.test, lastNPCTriggered.GetComponent<NPCController>().npcDialog);
			} else if (lastObjectTriggered.tag == "Item" && !watchingInventory){
				// Pick item
				lastObjectTriggered.GetComponent<Item>().OnPick();
			} else if (lastObjectTriggered.tag == "ItemContainer"){
				// Open item container inventory
				watchingInventory = FindObjectOfType<InventoryController>().ToggleNonBreakableContainer(lastObjectTriggered.GetComponent<ItemContainer>());
				lastObjectTriggered = null;
			}
		}
	}
	void OnToggleInventory(){
		watchingInventory = FindObjectOfType<InventoryController>().Toggle();
	}

	void onTriggerEnterEvent( Collider2D other ){
		
	}

	void onTriggerStayEvent( Collider2D other ){
		switch (other.tag){
			case "NPC":
			if(other.GetComponent<NPCController>().withDialog)
				uiController.collidingWithNPCObj.SetActive(true);
			lastNPCTriggered = other.gameObject;
			break;
			case "Item":
			lastObjectTriggered = other.gameObject;
			break;
			case "ItemContainer":
			if(other.gameObject.GetComponent<ItemContainer>().breakable){
				return;
			}
			lastObjectTriggered = other.gameObject;
			break;
		}
	}

	void onTriggerExitEvent( Collider2D other ){
		switch (other.tag){
			case "NPC":
			uiController.collidingWithNPCObj.SetActive(false);
			break;
		}
	}

	void onControllerCollider( RaycastHit2D hit ){
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
			return;

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}
}
