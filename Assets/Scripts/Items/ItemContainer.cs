﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer : MonoBehaviour {

	public List<ItemSO> items = new List<ItemSO>();
	public bool breakable = false;

	[HideInInspector]
	public bool broken = false;

	[SerializeField]
	private GameObject itemPf;
	[SerializeField]
	private Sprite containerBrokenSpr;

	private float itemPosOffset = 0.3f;
	private SpriteRenderer spriteRenderer = null;
	private Color transparent = new Color(1,1,1,0);

	void Awake(){
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void OnBreakContainer(){
		if(broken)
			return;
		foreach (ItemSO item in items){
			Vector3 pos = transform.position;
			pos.x += Random.Range(-itemPosOffset, itemPosOffset);
			pos.y += Random.Range(itemPosOffset, itemPosOffset);
			GameObject newItem = Instantiate(itemPf, pos, Quaternion.identity, transform.parent);
			newItem.GetComponent<Item>().item = item;
		}
		spriteRenderer.sprite = containerBrokenSpr;
		GetComponent<BoxCollider2D>().enabled = false;
		broken = true;
	}

	void Update(){
		if(broken){
			spriteRenderer.color = Color.Lerp(spriteRenderer.color, transparent, 10f * Time.deltaTime);
			if(spriteRenderer.color.a <= 0f){
				Destroy(gameObject);
			}
		}
	}
}
