﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="New item", menuName="Item")]
public class ItemSO : ScriptableObject {

	public int itemID;
	public string itemName;
	public Sprite inventoryImage;
	public Sprite worldImage;
	public bool consumable;
	public ItemConsumableTypes consumableType;
}

public enum ItemConsumableTypes{
	healing = 0,
	posion = 1,
	magic = 2,
}
