﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	// Public properties
	[HideInInspector]
	public int numberOfSlots = 0;
	[HideInInspector]
	public GameObject slotPf = null;
	[HideInInspector]
	public InventoryController inventoryController = null;
	[HideInInspector]
	public bool isContainerInventory = false;
	[HideInInspector]
	public List<InventorySlot> slots = new List<InventorySlot>();

	// Private properties
	private int slotSelected = 0;
	private Transform slotsGridParent = null;
	private Sprite unselectedSlotSpr = null, selectedSlotSpr = null;
	private int gridWidth = 0, gridHeigth = 0;
	private ItemContainer container = null;

	// Double click
	private float initialClick = 0;
	private float doubleClickDelay = 0.5f;
	private int lastSlotSelected = 0;

	// Initialize inventory
	public void Setup(){
		slotsGridParent = transform.GetChild(0);
		unselectedSlotSpr = Resources.Load<Sprite>("Sprites/Inventario/inventory_slot_unselected");
		selectedSlotSpr = Resources.Load<Sprite>("Sprites/Inventario/inventory_slot_selected");

		for (int i = 0; i < numberOfSlots; i++){
			GameObject slot = Instantiate(slotPf,Vector3.zero,Quaternion.identity,slotsGridParent);
			slot.GetComponent<Image>().sprite = unselectedSlotSpr;
			int id = i;
			slot.GetComponent<Button>().onClick.AddListener(() => {OnClickSlot(id);});
			if(!isContainerInventory){
				if(inventoryController.playerItems.Count > i){
					slot.GetComponent<InventorySlot>().itemOnSlot = inventoryController.playerItems[i];
				}
			}
			slot.GetComponent<InventorySlot>().Setup();
			slots.Add(slot.GetComponent<InventorySlot>());
			slot.SetActive(true);
		}
		gridWidth = Mathf.FloorToInt(slotsGridParent.GetComponent<RectTransform>().sizeDelta.x / (slotsGridParent.GetComponent<GridLayoutGroup>().cellSize.x + slotsGridParent.GetComponent<GridLayoutGroup>().spacing.x));
		gridHeigth = Mathf.FloorToInt(slotsGridParent.GetComponent<RectTransform>().sizeDelta.y / (slotsGridParent.GetComponent<GridLayoutGroup>().cellSize.y + slotsGridParent.GetComponent<GridLayoutGroup>().spacing.y));
		slotSelected = 0;
		//slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = selectedSlotSpr;
	}

	void Update(){
		if(inventoryController.inventorySelected == this){
			if(Input.GetButtonDown("ActionButton")){
				OnSelectSlot();
			} else if (Input.GetButtonDown("SwitchInventory")){
				inventoryController.switchInventory = true;
				slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = unselectedSlotSpr;
			}
		}
		if (Input.GetButtonDown("Back")){
			CloseInventory();
		}
		else if((Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical")) && inventoryController.inventorySelected == this){
			// Change selected slot with keys
			slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = unselectedSlotSpr;
			if(Input.GetAxisRaw("Horizontal") < 0){
				// Left
				slotSelected--;
				if(slotSelected < 0){
					slotSelected = slotsGridParent.childCount-1;
				}
			} else if (Input.GetAxisRaw("Horizontal") > 0){
				// Right
				slotSelected++;
				if(slotSelected >= slotsGridParent.childCount){
					slotSelected = 0;
				}
			} else if(Input.GetAxisRaw("Vertical") > 0){
				// Up
				slotSelected-=gridWidth;
				if(slotSelected < 0){
					slotSelected = slotsGridParent.childCount - (Mathf.Abs(slotSelected));
				}
			} else if (Input.GetAxisRaw("Vertical") < 0){
				// Down
				slotSelected+=gridWidth;
				if(slotSelected >= slotsGridParent.childCount){
					slotSelected = (gridWidth - (((gridWidth*gridHeigth)-(slotSelected)))) - gridWidth;
				}
			}
			// Set results
			slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = selectedSlotSpr;
		}
	}

	/// <summary>
	/// Called on click slot from this inventory
	/// </summary>
	/// <param name="slotSiblingID">The slot sibling(child) index</param>
	void OnClickSlot(int slotSiblingID){
		SetSlotSelected(slotSiblingID);
		if (Time.time < (initialClick + doubleClickDelay) && lastSlotSelected == slotSiblingID) {
			// Double click
			OnSelectSlot();
		}
		lastSlotSelected = slotSiblingID;
		initialClick = Time.time;
	}
	/// <summary>
	/// Called on select slot from this inventory
	/// </summary>
	void OnSelectSlot(){
		InventorySlot _slotSelected = slotsGridParent.GetChild(slotSelected).GetComponent<InventorySlot>();
		if(!isContainerInventory){
			// Player inventory
			inventoryController.OnSelectSlot(_slotSelected.itemOnSlot, _slotSelected);
			if(_slotSelected.itemOnSlot != null){
				if(inventoryController.IsNonBreakableContainerOpen()){
					// Switch inventory
					int id = _slotSelected.transform.GetSiblingIndex();
					inventoryController.playerItems[id] = null;
					_slotSelected.SetEmptySlot();
					_slotSelected.itemOnSlot = null;
				}
				else if(_slotSelected.itemOnSlot.consumable){
					// Consume item
					int id = _slotSelected.transform.GetSiblingIndex();
					inventoryController.playerItems[id] = null;
					_slotSelected.SetEmptySlot();
					_slotSelected.itemOnSlot = null;
				}
			}
		} else{
			// Container inventory
			if(_slotSelected.itemOnSlot != null){
				inventoryController.OnPickItem(_slotSelected.itemOnSlot);
				_slotSelected.SetEmptySlot();
				_slotSelected.Setup();
				container.items[_slotSelected.containerID] = null;
			}
		}
	}
	void CloseInventory(){
		inventoryController.CloseInventory();
	}
	/// <summary>
	/// Called from InventoryController on add item to player's inventory (from the world or from other inventory)
	/// </summary>
	/// <param name="id">The slot id to add the item</param>
	public void OnItemPicked(int id){
		slots[id].itemOnSlot = inventoryController.playerItems[id];
		slots[id].Setup();
	}
	/// <summary>
	/// Called from inventoryController on open a items container inventory
	/// </summary>
	/// <param name="container">The world container opened</param>
	public void OpenContainerInventory(ItemContainer container){
		this.container = container;
		foreach (InventorySlot slot in slots){
			slot.SetEmptySlot();
		}
		for (int i = 0; i < this.container.items.Count; i++){
			slots[i].itemOnSlot = this.container.items[i];
			slots[i].containerID = i;
			slots[i].Setup();
		}
	}
	/// <summary>
	/// Set one slot as the selected slot
	/// </summary>
	/// <param name="slotSiblingID">The sibling index(the child index) of the slot to mark as selected</param>
	public void SetSlotSelected(int slotSiblingID){
		slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = unselectedSlotSpr;
		slotSelected = slotSiblingID;
		slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = selectedSlotSpr;
	}
	/// <summary>
	/// Remove an item on this inventory
	/// </summary>
	/// <param name="slot">The slot where to remove the item</param>
	public void RemoveItemOnSlot(InventorySlot slot){
		if(isContainerInventory){
			slot.SetEmptySlot();
			slot.Setup();
			container.items[slot.containerID] = null;
		} else{
			slot.SetEmptySlot();
			slot.Setup();
			int id = slot.transform.GetSiblingIndex();
			inventoryController.playerItems[id] = null;
			slot.itemOnSlot = null;
		}
	}
	/// <summary>
	/// Called from InventoryController on drop item on slot
	/// </summary>
	/// <param name="slot">The original dragged slot</param>
	/// <param name="destID">The index of the slot where the player dropped the item</param>
	public void OnDropItem(InventorySlot slot, int destID){
		if(isContainerInventory){
			// Manage the world item container...
			while (destID > inventoryController.itemContainerOpened.items.Count-1){
				ItemSO item = null;
				inventoryController.itemContainerOpened.items.Add(item);
			}
			inventoryController.itemContainerOpened.items[destID] = slot.itemOnSlot;
			// Move the item to the container inventory and initialize it
			slots[destID].itemOnSlot = slot.itemOnSlot;
			slots[destID].containerID = destID;
			slots[destID].Setup();
		} else{
			inventoryController.OnPickItem(slot.itemOnSlot, destID);
		}
	}

	public void OnSelectThisInventory(){
		slotsGridParent.GetChild(slotSelected).GetComponent<Image>().sprite = selectedSlotSpr;
	}
}
