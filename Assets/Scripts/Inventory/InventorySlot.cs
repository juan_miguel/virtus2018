﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	//[HideInInspector]
	public ItemSO itemOnSlot = null;
	[HideInInspector]
	public int containerID = -1;

	private Image itemImage = null;
	private InventoryController inventoryController;

	void Awake(){
		inventoryController = FindObjectOfType<InventoryController>();
	}

	public void Setup(){
		if(itemImage == null){
			itemImage = transform.GetChild(0).GetComponent<Image>();
		}
		if(itemOnSlot)
			itemImage.sprite = itemOnSlot.inventoryImage;
		else
			itemImage.sprite = null;
	}
	public void SetEmptySlot(){
		itemImage.sprite = null;
		itemOnSlot = null;
	}

	public void OnBeginDrag(PointerEventData eventData){
		if(itemOnSlot)
			inventoryController.OnBeginDrag(this);
	}
	public void OnDrag(PointerEventData data){
		if(itemOnSlot)
			inventoryController.OnDrag(data.position);
    }
	public void OnEndDrag(PointerEventData eventData){
		if(itemOnSlot)
			inventoryController.OnEndDrag(eventData.position);
    }
}
