﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour {

	private static bool created = false;
	public static string cambiarEstado = "Menu",estadoJuego="",parametroEstado="";
	public static int contadorCiclos;
	public static int c5=0,c10=0,c20=0,c100=0;
	
	public static InventoryController.InventoryTypeID inventoryTypePlayer = InventoryController.InventoryTypeID.Medium;

	void Awake(){
		if (!created){
			DontDestroyOnLoad(this.gameObject);
			created = true;
		} else{
			Destroy(gameObject);
			return;
		}
		if(Application.systemLanguage == SystemLanguage.Spanish){
			Data.lang = "es";
		}
		SceneManager.sceneLoaded += OnSceneLoaded;
		SceneManager.LoadScene("Menu");
	}

	void Start(){
		contadorCiclos = 0;
		//Cambiar la resolucion
		//De momento no lo vamos a usar
		//Screen.SetResolution(1920, 1080, false);
	}

	void FixedUpdate(){
		contadorCiclos++;
		c5 = (contadorCiclos % 5);
		c10 = (contadorCiclos % 10);
		c20 = (contadorCiclos % 20);
		c100 = (contadorCiclos % 100);

		if(cambiarEstado!="")
		{
			Debug.Log("Loading Scene: " + cambiarEstado);
			SceneManager.LoadScene(cambiarEstado, LoadSceneMode.Single);
			estadoJuego = "" + cambiarEstado;
			cambiarEstado = "";
			contadorCiclos = 0;
		}
		else
		//Aqui gestiono los estados
		switch (estadoJuego)
		{
			case "Menu":
			if (Input.GetButton("Jump"))
			{
				parametroEstado = "1";
				cambiarEstado = "Nivel";
			}
			break;
			case "Nivel1":
			//Cambio de estado, quitar
			if (Input.GetButton("Jump"))
			{
				if(parametroEstado=="1")
					parametroEstado = "2";
				else
					parametroEstado = "1";
				cambiarEstado = "Nivel";
			}
			/*if (contadorCiclos == 1)
			{	
						
				GameObject ob;
				//Crea la primera nave
				ob=(GameObject)Instantiate(heroe, inicio1,Quaternion.identity);
				//Crea la segunda nave
				if (parametroEstado == "2")
				{
					ob=(GameObject)Instantiate(heroe, inicio2,Quaternion.identity);
					//Asi es como cambiamos un objeto
					ob.GetComponent<MeshFilter> ().sharedMesh = nave2;	
					//Aqui es donde cambiamos el control
					ob.GetComponent<HeroeControl> ().horizontal="Horizontal2";
					ob.GetComponent<HeroeControl> ().vertical="Vertical2";
					ob.GetComponent<HeroeControl> ().fire1="Fire1_2";
				}
				

			}*/
			break;
		}

	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		switch (scene.name)
		{
			case "Nivel":
			// Borrame
			//FindObjectOfType<UIController>().StartDialog(DialogController.DialogsIDs.test);
			break;
		}
	}
}