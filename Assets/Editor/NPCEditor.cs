/*******************************************************
 * Copyright (C) 2018-2019 David Basagaña Mimoso <davidbasaganya@gmail.com>
 * 
 * This file is part of Virtus Project.
 * 
 * This code can not be copied and/or distributed without the express
 * permission of David Basagaña Mimoso
 *******************************************************/

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(NPCController))]
public class NPCEditor : Editor{
    public override void OnInspectorGUI(){
        serializedObject.Update();
        var npcController = target as NPCController;

        if(!npcController.withDialog){
            Editor.DrawPropertiesExcluding(serializedObject, new string[]{"npcDialog"});
            serializedObject.ApplyModifiedProperties();
        } else{
            DrawDefaultInspector();
        }
    }
}