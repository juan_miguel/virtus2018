/*******************************************************
 * Copyright (C) 2018-2019 David Basagaña Mimoso <davidbasaganya@gmail.com>
 * 
 * This file is part of Virtus Project.
 * 
 * This code can not be copied and/or distributed without the express
 * permission of David Basagaña Mimoso
 *******************************************************/

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ItemSO))]
public class ItemSOEditor : Editor{
    public override void OnInspectorGUI(){
        serializedObject.Update();
        var npcController = target as ItemSO;

        if(!npcController.consumable){
            Editor.DrawPropertiesExcluding(serializedObject, new string[]{"consumableType"});
            serializedObject.ApplyModifiedProperties();
        } else{
            DrawDefaultInspector();
        }
    }
}