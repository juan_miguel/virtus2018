using System;

[Serializable]
public class Dialog{
    public string[] text;
    public string[] option1;
    public string[] option2;
    public int[] talkingCharacter;
}