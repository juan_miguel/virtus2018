﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	//[HideInInspector]
	public ItemSO item;

	void Awake(){
		GetComponent<SpriteRenderer>().sprite = item.worldImage;
	}

	public void OnPick(){
		if(FindObjectOfType<InventoryController>().OnPickItem(item)){
			Destroy(gameObject);
		} else{
			Debug.Log("Cannot add item to inventory!");
		}
	}
}
