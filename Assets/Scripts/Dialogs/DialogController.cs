﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogController : MonoBehaviour {

	[SerializeField]
	private TextMeshProUGUI dialogTxt, option1, option2;
	[SerializeField]
	private Image charLImg, charRImg;
	[Header("Parametros del dialogo")]
	[SerializeField]
	private float dialogSpeed = 0.2f;
	//[SerializeField]
	//private Color notSpeakingColor = Color.white;
	[HideInInspector]
	public bool init = false;

	private DialogCollection dialogCollection;
	private int charLength = 0;
	private int startCharID = 0;
	private DialogsIDs dialogID = 0;
	private int pageID = 0;
	private float dialogTimer = 0;
	private int state = 0;
	private int numberOfPossibleAnswers = 2;
	private int answerSelected = 1;
	private NPCDialog npcDialog = null;

	private UIController uiController = null;

	public enum DialogsIDs{
		test = 0,
	}

	public void Setup(NPCDialog npcDialog){
		// Link
		uiController = FindObjectOfType<UIController>();
		this.npcDialog = npcDialog;

		// Dialogs setup
		LoadDialogs();
		dialogID = DialogsIDs.test;
		dialogTxt.text = "";
		option1.gameObject.SetActive(false);
		option2.gameObject.SetActive(false);
		state = 99;
		init = true;
	}
	
	void Update(){
		switch (state){
			case 0:		// Wait
			if(dialogTxt.isTextTruncated){
				if(Input.GetMouseButtonDown(0)){
					// Continue
					charLength = 0;
					dialogTxt.text = "";
					state = 1;
				}
			} else{
				// Change to next page
				if(Input.GetButtonDown("Vertical")){
					ChangeSelector();
				} else if (Input.GetButtonDown("ActionButton")){
					OnChangePage(answerSelected);
				}
			}
			break;
			case 1:		// Play
			if((dialogTimer+=Time.deltaTime) > dialogSpeed){
				charLength++;
				if((startCharID + charLength) > dialogCollection.dialogs[(int)dialogID].text[pageID].Length){
					if((pageID + numberOfPossibleAnswers + pageID) < dialogCollection.dialogs[(int)dialogID].text.Length){
						option1.gameObject.SetActive(true);
						option2.gameObject.SetActive(true);
					}
					option1.transform.GetChild(0).gameObject.SetActive(true);
					option2.transform.GetChild(0).gameObject.SetActive(false);
					answerSelected = 1;
					state = 0;
				} else{
					dialogTxt.text = dialogCollection.dialogs[(int)dialogID].text[pageID].Substring(startCharID, charLength);
					if(dialogTxt.isTextTruncated){
						startCharID += charLength - ((dialogTxt.overflowMode == TextOverflowModes.Ellipsis) ? 4 : 2);
						state = 0;
					}
				}	
				dialogTimer = 0f;	
			}
			break;
		}
	}

	public void StartDialog(DialogsIDs dialogID){
		startCharID = charLength = pageID = 0;
		dialogTimer = 0;
		this.dialogID = dialogID;
		option1.text = dialogCollection.dialogs[(int)dialogID].option1[pageID];
		option2.text = dialogCollection.dialogs[(int)dialogID].option2[pageID];
		option1.gameObject.SetActive(false);
		option2.gameObject.SetActive(false);
		// Characters images
		if(dialogCollection.dialogs[(int)dialogID].talkingCharacter[pageID] == 0){
			// Left speaking
			charLImg.sprite = npcDialog.leftCharActive;
			charRImg.sprite = npcDialog.rightCharInactive;
		} else{
			// Right speaking
			charRImg.sprite = npcDialog.rightCharActive;
			charLImg.sprite = npcDialog.leftCharInactive;
		}
		state = 1;
	}

	void ChangeSelector(){
		option1.transform.GetChild(0).gameObject.SetActive(!option1.transform.GetChild(0).gameObject.activeInHierarchy);
		option2.transform.GetChild(0).gameObject.SetActive(!option2.transform.GetChild(0).gameObject.activeInHierarchy);
		answerSelected = option1.transform.GetChild(0).gameObject.activeInHierarchy ? 1 : 2;
	}

	void OnChangePage(int selectedAnswer){
		startCharID = 0;
		option1.gameObject.SetActive(false);
		option2.gameObject.SetActive(false);
		pageID += selectedAnswer + pageID;
		if(pageID >= dialogCollection.dialogs[(int)dialogID].text.Length){
			// End of dialog
			uiController.OnEndDialog();
		} else{
			// Hide answers if end of dialog
			if((pageID + numberOfPossibleAnswers + pageID) < dialogCollection.dialogs[(int)dialogID].text.Length){
				option1.text =  dialogCollection.dialogs[(int)dialogID].option1[pageID];
				option2.text =  dialogCollection.dialogs[(int)dialogID].option2[pageID];
			}
			// Characters images
			if(dialogCollection.dialogs[(int)dialogID].talkingCharacter[pageID] == 0){
				// Left speaking
				charLImg.sprite = npcDialog.leftCharActive;
				charRImg.sprite = npcDialog.rightCharInactive;
			} else{
				// Right speaking
				charRImg.sprite = npcDialog.rightCharActive;
				charLImg.sprite = npcDialog.leftCharInactive;
			}
			// Next page
			charLength = 0;
			dialogTxt.text = "";
			state = 1;
		}
	}

	private void LoadDialogs(){
		string dialogsPath = Application.dataPath + "/Resources/Text/"+Data.lang+"/Dialogs.json";
		using (StreamReader stream = new StreamReader(dialogsPath)){
			string json = stream.ReadToEnd();
			dialogCollection = JsonUtility.FromJson<DialogCollection>(json);
		}
	}
}
