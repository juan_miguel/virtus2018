﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapaControler : MonoBehaviour {

	//Mapas
	public TileBase tileOcupado,tileNivel1;
	private GameObject mapa;
	private Tilemap colision,fondo;
	private TilemapRenderer colisionRenderer;
	//
	public PlayerController ninjaControler;

	public TextAsset textoPrueba;

	public static int Ancho_Mapa=0,Alto_Mapa=0;
	//private Vector2 ninjaPos; 

	public readonly int TILE_OCUPADO=0;
	public readonly int TILE_NIVEL1=1;
	public readonly int TILE_NIVEL2=2;
	public readonly int TILE_RAMPA1=3;
	public readonly int TILE_RAMPA2=4;
	public readonly int TILE_SALTO1=5;
	public readonly int TILE_SALTO2=6;

	private Vector3Int tilePos= new Vector3Int(0,0,0);


	/*
	//Mapas
	public int[] mapa=null;
	private int[] mapa_1={
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,

		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,

		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,

		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
	};
	private int[] mapa_2 = {
		1,1,1,1,1,1,1,0,1,
		1,0,0,0,0,0,0,0,1,
		1,0,1,1,1,0,1,1,1,
		1,1,1,0,1,0,0,0,1,
		0,0,0,0,0,0,1,0,1,
		1,1,1,1,1,0,1,0,1,
		1,1,1,1,0,0,1,1,1,
		1,1,1,1,1,1,1,1,1,
	};


	//Te dice que hay en una casilla
	public int CasillaXY(float x,float y)
	{
		if ((x < 0f) || (y < 0f))
			return TILE_OCUPADO;
		int xx = (int)x;
		int yy = (int)y;
		if ((x >= Ancho_Mapa) || (y >= Alto_Mapa))
			return TILE_OCUPADO;
		//Aqui retorna el contenido
		return mapa[xx+((Alto_Mapa-yy-1)*Ancho_Mapa)];
	}
	*/


	//Te dice que hay en una casilla
	public int CasillaXY(float x,float y)
	{
		int xx = (int)x;
		int yy = (int)y;
		/*
		if ((x < 0f) || (y < 0f))
			return TILE_OCUPADO;
		if ((x >= Ancho_Mapa) || (y >= Alto_Mapa))
			return TILE_OCUPADO;
		*/
		//
		//Aqui retorna el contenido
		tilePos.x = xx;
		tilePos.y = yy;
		if (colision.GetTile(tilePos) == tileNivel1)
			return TILE_NIVEL1;
		else
			return TILE_OCUPADO;
	}


	public int CasillaXY_rango(float x,float y,float distancia,float desplaX, float desplaY)
	{
		x += desplaX;
		y += desplaY;
		int c1 = CasillaXY (x - distancia, y - distancia);
		int c2 = CasillaXY (x + distancia, y - distancia);
		int c3 = CasillaXY (x - distancia, y + distancia);
		int c4 = CasillaXY (x + distancia, y + distancia);
		if ((c1 == TILE_NIVEL1) && (c2 == TILE_NIVEL1) && (c3 == TILE_NIVEL1) && (c4 == TILE_NIVEL1))
			return TILE_NIVEL1;
		else
			return TILE_OCUPADO;
	}




	/*
	// Use this for initialization
	void Start () 
	{

		ninjaPos = new Vector2 (0f, 0f);
		string mapaCargar = "";
		//Aqui carga el mapa				
		if (GameControler.parametroEstado == "1")
		{
			mapaCargar = "Mapas/mapa_sq_bg_1_128";
			Ancho_Mapa = 28;
			Alto_Mapa = 20;
			ninjaControler.MoverNinja (0.5f, 0.5f);
			mapa = mapa_1;
		}
		if (GameControler.parametroEstado == "2")
		{
			mapaCargar = "Mapas/mapa_sq_bg_4_128";
			Ancho_Mapa = 9;
			Alto_Mapa = 8;
			ninjaControler.MoverNinja (0.5f, 0.5f);
			mapa = mapa_2;
		}
		//Carga el mapa
		SpriteRenderer sp = GetComponent<SpriteRenderer> ();
		sp.sprite = Resources.Load<Sprite>(mapaCargar);		
	}
	*/

	// Use this for initialization
	void Start () 
	{
		string mapaCargar = "";
		//Salida del ninja, algo mas arrba, para que no choque
		//ninjaControler.MoverNinja (1.5f, 1.5f+0.3f);
		//Aqui carga el mapa				
		mapaCargar = "Mapas/Niveles/Mapa_"+GameController.parametroEstado;
		/*if (GameControler.parametroEstado == "1")
		{
			mapaCargar = "Mapas/Niveles/mapa_tileado_01";
		}
		if (GameControler.parametroEstado == "2")
		{
			mapaCargar = "Mapas/Niveles/mapa_tileado_02";
		}*/
		/*
		//Carga el mapa
		Tilemap tl = Resources.Load<Tilemap>(mapaCargar);
		colision = tl.transform.Find("colision").gameObject.GetComponent<Tilemap>();
		fondo = tl.transform.Find("fondo").gameObject.GetComponent<Tilemap>();
		*/

		//Carga el mapa
		mapa=Instantiate(Resources.Load<GameObject>(mapaCargar));
		colision = mapa.transform.GetChild(0).Find("colision").gameObject.GetComponent<Tilemap>();
		fondo = mapa.transform.GetChild(0).Find("fondo").gameObject.GetComponent<Tilemap>();
		colisionRenderer = mapa.transform.GetChild(0).Find("colision").gameObject.GetComponent<TilemapRenderer>();

		//Ajustamos el tamaño al mapa
		Ancho_Mapa = colision.cellBounds.xMax;
		Alto_Mapa = colision.cellBounds.yMax;

	}


	/*void FixedUpdate()
	{		
		if (GameController.c100 < 50)
			colisionRenderer.enabled = true;
		else
			colisionRenderer.enabled = false;
	}*/
		
	
	// Update is called once per frame
	void Update ()
	{
	}
}
